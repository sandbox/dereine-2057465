<?php

/**
 * @file
 * Contains \Drupal\config_git_storage\Config\GitStorageFactory.
 */

namespace Drupal\config_git_storage\Config;

use Drupal\Component\Utility\Settings;
use Symfony\Component\HttpFoundation\Request;

class GitStorageFactory {

  /**
   * Returns a FileStorage object working with the active config directory.
   *
   * @param \Drupal\Component\Utility\Settings $settings
   *  The settings.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   (optional) The current request.
   *
   * @return \Drupal\config_git_storage\Config\GitStorage
   */
  static function getActive(Settings $settings, Request $request = NULL) {
    return new GitStorage(config_get_config_directory(CONFIG_ACTIVE_DIRECTORY), $settings);
  }

  /**
   * Returns a FileStorage object working with the staging config directory.
   *
   * @param \Drupal\Component\Utility\Settings $settings
   *   The settings.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   (optional) The current request.
   *
   * @return \Drupal\config_git_storage\Config\GitStorage
   */
  static function getStaging(Settings $settings, Request $request = NULL) {
    return new GitStorage(config_get_config_directory(CONFIG_STAGING_DIRECTORY), $settings);
  }

}
