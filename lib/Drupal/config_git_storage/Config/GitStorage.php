<?php

/**
 * @file
 * Contains \Drupal\config_git_storage\Config\GitStorage.
 */

namespace Drupal\config_git_storage\Config;

use Drupal\Component\Utility\Settings;
use Drupal\Core\Config\FileStorage;
use GitWrapper\GitException;
use GitWrapper\GitWorkingCopy;
use GitWrapper\GitWrapper;
use Symfony\Component\HttpFoundation\Request;

$loader = drupal_classloader();
$prefixes_and_namespaces = require __DIR__ . '/../../../../vendor/composer/autoload_namespaces.php';
$loader->addPrefixes($prefixes_and_namespaces);
$loader->register();

class GitStorage extends FileStorage {

  /**
   * Stores the used git working copy handler.
   *
   * @var \GitWrapper\GitWorkingCopy.
   */
  protected $gitWorkingCopy;

  /**
   * The drupal settings.
   *
   * @var \Drupal\Component\Utility\Settings
   */
  protected $settings;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a new GitStorage object.
   *
   * @param string $directory
   *   A directory path to use for reading and writing of configuration files.
   * @param \Drupal\Component\Utility\Settings $settings
   *   The drupal settings.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   (optional) The current request.
   */
  public function __construct($directory, Settings $settings, Request $request = NULL) {
    parent::__construct($directory);

    $this->settings = $settings;
    $this->request = $request;
  }

  /**
   * Initializes the git rep and sets ups the git configuration for the user.
   */
  protected function init() {
    $git_wrapper = new GitWrapper();
    $this->gitWorkingCopy = new GitWorkingCopy($git_wrapper, $this->directory);

    $this->gitWorkingCopy->init();

    $git_config = $this->read('config_git_storage.settings');
    $this->gitWorkingCopy
      ->config('user.name', isset($git_config['git']['user_name']) ? $git_config['git']['user_name'] : 'Drupal')
      ->config('user.email', isset($git_config['git']['user_mail']) ? $git_config['git']['user_mail'] : 'git@example.com');
  }

  /**
   * Gets the git author string from settings or current active user.
   *
   * @return string|null
   *   Returns the author string or NULL if not wanted.
   */
  protected function getGitAuthor() {
    /** @var $_account \Drupal\Core\Session\AccountInterface */
    if ($this->request && $_account = $this->request->attributes->get('_account')) {
      $uid = $_account->id();
      // We assume that developers use uid 1, so we want to be able to override
      // it via the settings.php
      if ($uid == 1) {
        $user_name = $this->settings->get('config_git_storage.user_name', $_account->getUsername());
        $user_mail = $this->settings->get('config_git_storage.user_mail', $_account->getEmail());
      }
      else {
        $user_name = $_account->getUsername();
        $user_mail = $_account->getEmail();
      }
    }
    else {
      $user_name = $this->settings->get('config_git_storage.user_name', NULL);
      $user_mail = $this->settings->get('config_git_storage.user_mail', NULL);
    }

    if ($user_name && $user_mail) {
      return "$user_name <$user_mail>";
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function write($name, array $data) {
    $result = parent::write($name, $data);

    $this->init();
    $this->gitWorkingCopy->add($this->getRelativeFilePath($name));

    // Ensure that commit is just called when there are changes.
    $this->gitWorkingCopy->clearOutput();
    $this->gitWorkingCopy->diff(NULL, array('cached' => TRUE));
    $output = $this->gitWorkingCopy->getOutput();

    if ($output) {
      $this->gitWorkingCopy->clearOutput();
      $request_time = strtotime(date('r', REQUEST_TIME));

      try {
        $this->gitWorkingCopy->log(NULL, array(1 => TRUE, 'format' => '%ad'));
        $last_commit_date = strtotime(trim($this->gitWorkingCopy->getOutput()));
      }
        // There is a git exception thrown when no initial commit happened
        // before, so let's do that.
      catch (GitException $e) {
        $this->gitCommit('Initial commit');
        $last_commit_date = $request_time;
      }

      $options = array(
        'date' => $request_time,
      );

      if ($last_commit_date == $request_time) {
        $options['amend'] = TRUE;
        $this->gitWorkingCopy->clearOutput();
        $this->gitWorkingCopy->run(array('diff HEAD~1', array('name-only' => TRUE)));
        $diff = $this->gitWorkingCopy->getOutput();
        $filelist = array_filter(array_map('trim', (explode("\n", $diff))));
        $filelist[] = $name . '.yml';
        asort($filelist);
        $this->gitCommit("Multiple config files written.\n\n" . implode("\n", $filelist), $options);
      }
      else {
        $this->gitCommit("Config written: $name.yml", $options);
      }
    }

    return $result;
  }

  /**
   * Gets the relative file path.
   *
   * The relative file path does contain the filename but not the config
   * directory in the files directory.
   */
  public function getRelativeFilePath($name) {
    return $name . '.' . static::getFileExtension();
  }

  /**
   * {@inheritdoc}
   */
  public function delete($name, $commit = TRUE) {
    $result = parent::delete($name);

    $this->init();
    $this->gitWorkingCopy->add($this->getRelativeFilePath($name), array('u' => NULL));

    if ($commit) {
      $this->gitCommit("Config removed: $name.yml");
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll($prefix = '') {
    $success = TRUE;
    $files = $this->listAll($prefix);
    foreach ($files as $name) {
      if (!$this->delete($name, FALSE) && $success) {
        $success = FALSE;
      }
    }
    $files = "* " . implode('\n * ', array_values($files)) . "*";
    $this->gitCommit("Removed all configuration by prefix: $prefix.\n\nThe following files got removed $files");

    return $success;
  }

  /**
   * Wraps the git command to add author information.
   *
   * @param string $message
   *   The commit message.
   * @param array $options
   *   (optional) The options for the commit, defaults to an empty array.
   */
  protected function gitCommit($message, $options = array()) {
    if ($author = $this->getGitAuthor()) {
      $options['author'] = $author;
    }
    $options['m'] = $message;
    $this->gitWorkingCopy->commit($options);
  }

}
